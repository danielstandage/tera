#!/usr/bin/env perl
use strict;
use Getopt::Long;
use List::Util qw(sum);
use Statistics::Descriptive;

sub print_usage
{
  print "Usage: $0 [options] < maker.gff
  Options:
    --groups=FILENAME    filename to which summary stats for each TE/repeat
                         category or group will be written; leave blank to
                         disable, or use 'stdout' to write to the terminal
                         standard output
    --help               print this message and exit
    --sort=STRING        value by which to sort the output; possible values are
                         'count', which sorts by the number of times each
                         TE/repeat type occurs, and 'length', which sorts by the
                         combined length of each TE/repeat type; default is
                         'count'
    --types=FILENAME     filename to which summary stats for each TE/repeat type
                         will be written; leave blank to disable, or use 'stdout'
                         to write to the terminal standard output
";
}

my $genus_counts = {};
my $species_counts = {};
my $genus_lengths = {};
my $species_lengths = {};

my $genus_file = "";
my $species_file = "";
my $sort_method = "count";

GetOptions
(
  "groups=s" => \$genus_file,
  "help"     => sub { print_usage(\*STDOUT); exit(0); },
  "sort=s"   => \$sort_method,
  "types=s"  => \$species_file,
);

while(my $line = <STDIN>)
{
  next if($line =~ m/\tmatch\t/);
  if($line =~ m/species:([^;%\s]+).*genus:([^;\s]+)/)
  {
    my $species = $1;
    my $genus = $2;
    $genus =~ s/\%25/%/g;
    $genus =~ s/\%(..)/chr(hex($1))/eg;
    my $name = "$genus:$species";

    my @values = split(/\t/, $line);
    my $length = $values[4] - $values[3] + 1;
    #printf("%s\t%s\t%d\n", $genus, $species, $length);
    
    $genus_counts->{$genus}   = 0  unless($genus_counts->{$genus});
    $species_counts->{$name}  = 0  unless($species_counts->{$name});
    $genus_lengths->{$genus}  = [] unless($genus_lengths->{$genus});
    $species_lengths->{$name} = [] unless($species_lengths->{$name});

    $genus_counts->{$genus}  += 1;
    $species_counts->{$name} += 1;
    push(@{$genus_lengths->{$genus}}, $length);
    push(@{$species_lengths->{$name}}, $length);
  }
}

if($genus_file ne "")
{
  my $data = {};
  foreach my $genus( keys(%$genus_counts) )
  {
    my $count = $genus_counts->{$genus};
    my $lengths = $genus_lengths->{$genus};
    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@$lengths);
    my $mean_length = $stat->mean;
    my @quartiles;
    foreach my $q(0..4)
    {
      push(@quartiles, $stat->quantile($q));
    }
    my $combined_length = sum(@$lengths);

    $data->{$genus}->{"combined_length"} = $combined_length;
    $data->{$genus}->{"mean_length"} = $mean_length;
    $data->{$genus}->{"length_distribution"} = join(',', @quartiles);
    $data->{$genus}->{"count"} = $count;
  }

  my $GNS;
  if($genus_file ne "stdout")
  {
    open($GNS, ">", $genus_file) or die("error: unable to open outfile '$genus_file'");
  }
  print($GNS "#Group name\tCount\tCombined length\tMean length\tLength distribution (quartiles)\n");

  sub by_count_genus
  {
    return $data->{$b}->{"count"} <=> $data->{$a}->{"count"};
  }
  sub by_length_genus
  {
    return $data->{$b}->{"combined_length"} <=> $data->{$a}->{"combined_length"};
  }
  my @genus_names;
  if($sort_method eq "count")
  {
    @genus_names = sort by_count_genus keys(%$data);
  }
  elsif($sort_method eq "length")
  {
    @genus_names = sort by_length_genus keys(%$data);
  }
  else
  {
    printf(STDERR "warning: sort method '$sort_method' unsupported\n");
    @genus_names = keys(%$data);
  }
  foreach my $genus(@genus_names)
  {
    printf($GNS "%s\t%d\t%d\t%.2f\t[%s]\n", $genus, $data->{$genus}->{"count"}, $data->{$genus}->{"combined_length"}, $data->{$genus}->{"mean_length"}, $data->{$genus}->{"length_distribution"});
  }
}

if($species_file ne "")
{
  my $data = {};
  foreach my $species( keys(%$species_counts) )
  {
    my $count = $species_counts->{$species};
    my $lengths = $species_lengths->{$species};
    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@$lengths);
    my $mean_length = $stat->mean;
    my @quartiles;
    foreach my $q(0..4)
    {
      push(@quartiles, $stat->quantile($q));
    }
    my $combined_length = sum(@$lengths);

    $data->{$species}->{"combined_length"} = $combined_length;
    $data->{$species}->{"mean_length"} = $mean_length;
    $data->{$species}->{"length_distribution"} = join(',', @quartiles);
    $data->{$species}->{"count"} = $count;
  }

  my $SPE;
  if($species_file ne "stdout")
  {
    open($SPE, ">", $species_file) or die("error: unable to open outfile '$species_file'");
  }
  print($SPE "#TE/repeat type\tCount\tCombined length\tMean length\tLength distribution (quartiles)\n");

  sub by_count_species
  {
    return $data->{$b}->{"count"} <=> $data->{$a}->{"count"};
  }
  sub by_length_species
  {
    return $data->{$b}->{"combined_length"} <=> $data->{$a}->{"combined_length"};
  }
  my @species_names;
  if($sort_method eq "count")
  {
    @species_names = sort by_count_species keys(%$data);
  }
  elsif($sort_method eq "length")
  {
    @species_names = sort by_length_species keys(%$data);
  }
  else
  {
    printf(STDERR "warning: sort method '$sort_method' unsupported\n");
    @species_names = keys(%$data);
  }
  foreach my $species(@species_names)
  {
    printf($SPE "%s\t%d\t%d\t%.2f\t[%s]\n", $species, $data->{$species}->{"count"}, $data->{$species}->{"combined_length"}, $data->{$species}->{"mean_length"}, $data->{$species}->{"length_distribution"});
  }
}

